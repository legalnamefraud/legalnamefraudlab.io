## Get Started

* [Birth Certificate Fraud; Clausula Rebus Sic Stantibus](/page/bccrss)
* [Legal Name Fraud Explained](/page/lnfexplained)

## Radio Shows

* [Legal Name Fraud Radio](https://blogtalkradio.com/legalnamefraudradio)
* [Archives By Lightworkers Media](https://www.youtube.com/watch?v=cJ0dvRKjyYg&list=PLGri4xAXHQXStKJvkGh2ayqNcV8dTp5DV)
